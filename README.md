# Código gerado durante o curso de gitlab-ci do Rodrigo Martinez da Rocha do missão DevOps

**Página do curso:**
http://missaodevops.com.br/docs/gitlabci_home.html

**Link para o curso na Udemy:**
https://www.udemy.com/course/gitlab-ci-pipelines-continuous-delivery-e-deployment/

O objetivo principal do curso é fazer deploy de um site estático utilizando o pipeline do gitlab criando, editando e entendendo o arquivo .gitlab-ci.yml
